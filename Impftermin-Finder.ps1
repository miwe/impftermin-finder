[CmdletBinding()]
param (
    [Parameter()]
    [System.Management.Automation.PSCredential]
    $Script:Credential,

    [Parameter()]
    [Hashtable]
    $TelegramNotificationOptions = @{
        #url = "https://api.telegram.org/bot<apikey>/sendMessage"
        #chat_id = "123456789"
    }
)

function Logon() { # returns the access_token
    $logonUrl = "https://signin.corona-impfung.nrw/adfs/oauth2/authorize?response_type=id_token token&response_mode=fragment&client_id=https://Kvno.Impftermin.Portal&scope=openid&resource=https://Kvno.Impftermin.Portal.WebApi&nonce=$(Get-Random -Minimum 1000000000 -Maximum 9999999999)&pullStatus=1&redirect_uri=https://termin.corona-impfung.nrw/terminreservierung&client-request-id=$(New-GUID)"

    if (-not $Script:Credential) {
        $Script:Credential = Get-Credential -Message "Anmeldedaten für corona-impfung.nrw"
    }

    $logonParams = @{
        "UserName" = $Script:Credential.Username;
        "Password" = $Script:Credential.GetNetworkCredential().Password;
        "AuthMethod" = 'FormsAuthentication';
    }
    $logonResult = Invoke-WebRequest -Uri $logonUrl -Method Post -Body $logonParams -SessionVariable session -TimeoutSec 10 -UseBasicParsing -ErrorAction SilentlyContinue

    if ($logonResult.StatusCode -ne 200 -and $session.Cookies.GetCookies($logonUrl).Count -ne 4) { # successful logon = 4 cookies
        Throw "Logon failed"
    }

    # access_token herausfinden
    try {
        $logonResult2 = Invoke-WebRequest -Uri $logonUrl -Method Get -WebSession $session -UseBasicParsing -ErrorAction SilentlyContinue -MaximumRedirection 0
        $access_token = ( ( ([uri]$logonResult2.Headers.Location).Fragment -split '&' | Where-Object { $_ -like "#access_token=*" } ) -split '#access_token=')[1]
    } catch {
        $access_token = ( ( ([uri]$Error[0].Exception.Response.Headers.Location).Fragment -split '&' | Where-Object { $_ -like "#access_token=*" } ) -split '#access_token=')[1]
    }

    if (-not $access_token) {
        Throw "No valid access_token"
    }

    $access_token
}

$access_token = Logon

$TermineUrl = 'https://termin.corona-impfung.nrw/api/impfzentrum/freietage'

$TermineParams = @{
    Von = (Get-Date -Format 'yyyy-MM-dd'); # today
    Bis = '2021-10-31';
    ImpfzentrumId = 4; # Eisporthalle Aachen
}

$telegramParams = @{
    chat_id = $TelegramNotificationOptions.chat_id
    text = "Terminfindung wird gestartet für $($Credential.Username) (Impfzentrum: Eissporthalle Aachen)"
}
Invoke-WebRequest -UseBasicParsing -Uri $TelegramNotificationOptions.Url -Method Post -Body $telegramParams | Out-Null

$start = Get-Date
while ($true) {

    # Get a new logon every 15 minutes
    if ( ((Get-Date) - $start).TotalMinutes -gt 15) {
        Write-Host -ForegroundColor Magenta "Renew Logon..."
        $access_token = Logon
        $start = Get-Date
    }

    Write-Host -ForegroundColor Yellow -NoNewline "$(Get-Date -Format 'yyyy-MM-dd HH:mm:ss') - Prüfe Termine... "
    try {
        $TermineResult = Invoke-WebRequest -Uri $TermineUrl -Method Post -ErrorAction SilentlyContinue -UseBasicParsing -TimeoutSec 10 -Headers @{Authorization = "Bearer $access_token"} -ContentType "application/json" -Body ($TermineParams | ConvertTo-Json)

        $content = $TermineResult.Content | ConvertFrom-Json
        if ( $content.Count -gt 0 ) {
            Write-Host -ForegroundColor Green "Termine frei :> ( $($content) ) (HTTP-Status Code: $($TermineResult.StatusCode))"
            $telegramParams = @{
                chat_id = $TelegramNotificationOptions.chat_id
                text = "Termine frei :> (Termine: $($Content))"
            }
            $content | ForEach-Object {
                $telegramParams.text += "`n$_"
            }
            Invoke-WebRequest -UseBasicParsing -Uri $TelegramNotificationOptions.Url -Method Post -Body $telegramParams | Out-Null
        } else {
            Write-Host -ForegroundColor Red "Keine Termine :< (Content: $($TermineResult.Content)) (HTTP-Status Code: $($TermineResult.StatusCode)) "
        }

    } catch {
        Write-Host -ForegroundColor Red "Fehler! (HTTP-Status Code: $($TermineResult.StatusCode)) "
        $telegramParams = @{
            chat_id = $TelegramNotificationOptions.chat_id
            text = "Fehler bei Überprüfung der freien Termine (Fehlercode: )"
        }
        Invoke-WebRequest -UseBasicParsing -Uri $TelegramNotificationOptions.Url -Method Post -Body $telegramParams | Out-Null
    }

    Start-Sleep 10
}


<# 

# Andere interessante URLs
'https://termin.corona-impfung.nrw/api/settings/auth'
'https://termin.corona-impfung.nrw/api/user'
'https://termin.corona-impfung.nrw/api/settings'
'https://termin.corona-impfung.nrw/api/impfling/buchung/backend/ok'
'https://termin.corona-impfung.nrw/api/impfling/buchung/status'
'https://termin.corona-impfung.nrw/api/settings/terminreservierung'
'https://termin.corona-impfung.nrw/api/benutzer/benutzer'
'https://termin.corona-impfung.nrw/api/impfzentrum/list'
 #>
